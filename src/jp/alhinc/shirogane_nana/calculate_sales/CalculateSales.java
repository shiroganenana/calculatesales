package jp.alhinc.shirogane_nana.calculate_sales;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.FilenameFilter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CalculateSales {
	public static void main(String args[]) {
		
		Map<String, String> branchName = new HashMap<String, String>();
		Map<String, Long> salesTotal = new HashMap<String, Long>();
		
		//支店定義ファイル読み込み
		BufferedReader branchBr = null;
		try {
			File branchFile = new File(args[0], "branch.lst");
			FileReader branchFileFr = new FileReader(branchFile);
			branchBr = new BufferedReader(branchFileFr);

			String branch;
			while ((branch = branchBr.readLine()) != null) {
				String[] code = branch.split(",");
				if (code.length != 2 || !(code[0].matches("^\\d{3}$"))){  //エラー処理1-2
					System.out.println("支店定義ファイルのフォーマットが不正です");
					return;
				}
				branchName.put(code[0], code[1]);  //code[0]=支店コード、code[1]=支店名
				salesTotal.put(code[0], 0L);
			}
			
		} catch (IOException e) {  //エラー処理1-1
			System.out.println("支店定義ファイルが存在しません");
			return;
		} finally {
			if (branchBr != null) {
				try {
					branchBr.close();
				} catch (IOException e) {
					System.out.println("予期せぬエラーが発生しました");
					return;
				}
			}
		}

		//集計
		FilenameFilter rcdFile = new FilenameFilter() {
			public boolean accept(File file, String str) {
				if (str.matches("^\\d{8}.rcd")) {  //ファイルの検索
					return true;
				} else {
					return false;
				}
			}
		};
		File[] salesFile = new File(args[0]).listFiles(rcdFile);
		String salesFileNumber = salesFile[0].getName().substring(0, salesFile[0].getName().lastIndexOf("."));
		int number = Integer.parseInt(salesFileNumber);
		for(int j = 0; j < salesFile.length; j++) {
			String compareSalesFileNumber = salesFile[j].getName().substring(0, salesFile[j].getName().lastIndexOf("."));
			int compareNumber = Integer.parseInt(compareSalesFileNumber);
			if (number != compareNumber) {  //エラー処理2-1
				System.out.println("売上ファイル名が連番になっていません");
				return;
			}
				number++;
		}
		
		for (int i = 0; i < salesFile.length; i++) {
			BufferedReader lineBr = null;
			try {
				File fileName = new File(args[0], salesFile[i].getName());
				FileReader fileNameFr = new FileReader(fileName);
				lineBr = new BufferedReader(fileNameFr);
			
				String line;
				List<String> salesList = new ArrayList<String>();
				while ((line = lineBr.readLine()) != null) {  //行の読み込み
					salesList.add(line);
				}
				if (salesList.size() > 2) {  //エラー処理2-4
					System.out.println(salesFile[i].getName() + "のフォーマットが不正です");
					return;
				}
			
				Long sales = Long.parseLong(salesList.get(1));  //salesList.get(1)=売上、Long型に変換
				if (salesTotal.containsKey(salesList.get(0))) {  //salesList.get(0)=支店コード
					Long sum = sales + salesTotal.get(salesList.get(0));//売上額の加算
					salesTotal.put(salesList.get(0), sum);
				}
				if (String.valueOf(salesTotal.get(salesList.get(0))).length() > 10) { //エラー処理2-2
					System.out.println("合計金額が10桁を超えました");
					return;
				}
				if(!(branchName.containsKey(salesList.get(0)))) { //エラー処理2-3
					System.out.println(salesFile[i].getName() + "の支店コードが不正です");
					return;
				}
			} catch (IOException e) {
				System.out.println("予期せぬエラーが発生しました");
				return;
			} finally {
				if (lineBr != null) {
					try {
						lineBr.close();
					} catch(IOException e) {
						System.out.println("予期せぬエラーが発生しました");
						return;
					}
				}
			}
		}
			
		//集計結果出力
		try {
			File salesOutput = new File(args[0],"branch.out");
			FileWriter salesOutputFw = new FileWriter(salesOutput);
			BufferedWriter branchBw = new BufferedWriter(salesOutputFw);
			for (String branchNameKey : branchName.keySet()) {  //branchNameのkey取得
				branchBw.write(branchNameKey + "," + branchName.get(branchNameKey) + "," + salesTotal.get(branchNameKey) + "\r\n");
			}
			branchBw.close();
		} catch (IOException e) {
			System.out.println("予期せぬエラーが発生しました");
			return;
		}
	}
}